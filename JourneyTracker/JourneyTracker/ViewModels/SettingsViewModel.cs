﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;

namespace JourneyTracker.ViewModels
{
    public class SettingsViewModel : INotifyPropertyChanged
    {
        public int radius;
        public int Radius 
        { 
            get { return radius; } 
            set 
            {
                radius = value;
                OnPropertyChanged();
            }
        }

        public string Title { get; set; } = "Settings";
        public SettingsViewModel()
        {
            Radius = 10;
        }

        //for xamarin forms when something changed
        public event PropertyChangedEventHandler PropertyChanged;

        void OnPropertyChanged([CallerMemberName]string propertyName ="")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
