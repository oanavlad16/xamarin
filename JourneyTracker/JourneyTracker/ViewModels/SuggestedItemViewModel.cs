﻿using JourneyTracker.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace JourneyTracker.ViewModels
{
    public class SuggestedItemViewModel : BaseViewModel
    {
        public Journey Item { get; set; }

        public SuggestedItemViewModel(Location location = null)
        {
            Title = location?.Name;
            Item = new Journey
            {
                Title = location?.Name,
                Date = DateTime.Now, 
                Icon = location.Icon
            };
        }
    }
}
