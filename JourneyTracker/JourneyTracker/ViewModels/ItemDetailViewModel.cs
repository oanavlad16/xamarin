﻿using System;

using JourneyTracker.Models;

namespace JourneyTracker.ViewModels
{
    public class ItemDetailViewModel : BaseViewModel
    {
        public Journey Item { get; set; }
        public ItemDetailViewModel(Journey item = null)
        {
            Title = item?.Title + " - Journey";
            Item = item;
        }
    }
}
