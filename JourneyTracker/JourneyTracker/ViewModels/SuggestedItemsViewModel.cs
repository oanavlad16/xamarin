﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading.Tasks;

using Xamarin.Forms;

using JourneyTracker.Models;
using JourneyTracker.Views;
using System.Collections.Generic;

namespace JourneyTracker.ViewModels
{
    public class SuggestedItemsViewModel : BaseViewModel
    {
        public ObservableCollection<Location> Items { get; set; }
        public Command LoadItemsCommand { get; set; }

        public SuggestedItemsViewModel(List<Location> lista)
        {
            Title = "Suggestions";
            Items = new ObservableCollection<Location>(lista);
            LoadItemsCommand = new Command(async () => await ExecuteLoadItemsCommand());
        }

        async Task ExecuteLoadItemsCommand()
        {
            IsBusy = true;

            try
            {
                //Items.Clear();
                //var items = await DataStore.GetItemsAsync(true);
                //foreach (var item in items)
                //{
                //    Items.Add(item);
                //}
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
    }
}