﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using JourneyTracker.Models;
using JourneyTracker.Views;
using JourneyTracker.ViewModels;
using JourneyTracker.Database;
using System.Collections.ObjectModel;
using Xamarin.Essentials;
using Location = JourneyTracker.Models.Location;
using DeviceLocation = Xamarin.Essentials.Location;

namespace JourneyTracker.Views
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class ItemsPage : ContentPage
    {
        ItemsViewModel viewModel;
        DatabaseContext databaseContext = DatabaseContext.GetInstance();

        public ItemsPage()
        {
            InitializeComponent();
        }

        async void OnItemSelected(object sender, EventArgs args)
        {
            var layout = (BindableObject)sender;
            var item = (Journey)layout.BindingContext;
            await Navigation.PushAsync(new ItemDetailPage(new ItemDetailViewModel(item)));
        }

        async void AddItem_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushModalAsync(new NavigationPage(new SuggestedLocationsPage()));
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();

            DeviceLocation location = await GetUserLocation();
            if(location != null)
            {
                Preferences.Set("lat", location.Latitude);
                Preferences.Set("lng", location.Longitude);
            }

            var lista = databaseContext.GetAll();
            BindingContext = viewModel = new ItemsViewModel(new ObservableCollection<Journey>(lista));

            if (viewModel.Items.Count == 0)
                viewModel.IsBusy = true;
        }

        async Task<DeviceLocation> GetUserLocation()
        {
            DeviceLocation location = null;
            try
            {
                var request = new GeolocationRequest(GeolocationAccuracy.Medium);
                location = await Geolocation.GetLocationAsync(request);
                return location;
            }
            catch (FeatureNotSupportedException fnsEx)
            {
                // Handle not supported on device exception
                return location;

            }
            catch (FeatureNotEnabledException fneEx)
            {
                // Handle not enabled on device exception
                return location;

            }
            catch (PermissionException pEx)
            {
                // Handle permission exception
                return location;

            }
            catch (Exception ex)
            {
                // Unable to get location
                return location;

            }
        }
    }
}