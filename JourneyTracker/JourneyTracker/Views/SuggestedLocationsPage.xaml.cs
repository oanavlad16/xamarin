﻿using JourneyTracker.Models;
using JourneyTracker.PlacesAPI;
using JourneyTracker.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace JourneyTracker.Views
{
    [DesignTimeVisible(false)]
    public partial class SuggestedLocationsPage : ContentPage
    {
        SuggestedItemsViewModel viewModel;
        List<Location> lista = new List<Location>();

        public SuggestedLocationsPage()
        {
            InitializeComponent();
            //request

            entryLocationName.TextChanged += EntryLocationName_TextChanged;

            BindingContext = viewModel = new SuggestedItemsViewModel(lista);
        }

        private async void EntryLocationName_TextChanged(object sender, TextChangedEventArgs e)
        {
            Entry entry = sender as Entry;
            String keyword = entry.Text;

            lista = await GoogleAPI.InitializeazaCurs(keyword);
            suggestionsLV.ItemsSource = new ObservableCollection<Location>(lista);
            
        }

        //cand e selectat un item deschidem ala cu salve si cancel deci NewItemPage
        async void OnItemSelected(object sender, EventArgs args)
        {
            var layout = (BindableObject)sender;
            var item = (Location)layout.BindingContext;
            await Navigation.PushAsync(new NewItemPage(new SuggestedItemViewModel(item)));
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();

            lista = await GoogleAPI.InitializeazaCurs("");

            suggestionsLV.ItemsSource = lista;

            if (viewModel.Items.Count == 0)
                viewModel.IsBusy = true;
        }

    }
}