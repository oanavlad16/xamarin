﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using JourneyTracker.Models;
using JourneyTracker.ViewModels;
using JourneyTracker.Database;

namespace JourneyTracker.Views
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class NewItemPage : ContentPage
    {
        SuggestedItemViewModel viewModel;
        DatabaseContext databaseContext = DatabaseContext.GetInstance();

        public NewItemPage(SuggestedItemViewModel viewModel)
        {
            InitializeComponent();
            BindingContext = this.viewModel = viewModel;

        }

        async void Save_Clicked(object sender, EventArgs e)
        {
            databaseContext.AddJourney(viewModel.Item);
            await Navigation.PopModalAsync();
        }

        async void Cancel_Clicked(object sender, EventArgs e)
        {
            await Navigation.PopModalAsync();
        }
    }
}