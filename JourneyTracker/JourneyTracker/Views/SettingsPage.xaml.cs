﻿using System;
using System.ComponentModel;
using Xamarin.Forms;

using JourneyTracker.ViewModels;
using Xamarin.Essentials;

namespace JourneyTracker.Views
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class SettingsPage : ContentPage
    {
        public SettingsPage()
        {
            InitializeComponent();

            BindingContext = new SettingsViewModel();
        }

        void OnSliderValueChanged(object sender, EventArgs e)
        {
            Slider slider = sender as Slider;
            int radius = Convert.ToInt32(slider.Value);

            //save as sharedpreferences
            Preferences.Set("radius", radius);
        }
    }
}