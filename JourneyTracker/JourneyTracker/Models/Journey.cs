﻿using System;
using System.Collections.Generic;
using System.Text;
using SQLite;

namespace JourneyTracker.Models
{
    [Table("Journey")]
    public class Journey
    {
        [PrimaryKey, AutoIncrement, Column("_id")]
        public long Id { get; set; }

        public String Title { get; set; }
        public DateTime Date { get; set; }
        public int Rating { get; set; }
        public String Notes { get; set; }

        public String Icon { get; set; }
        public override string ToString()
        {
            return Title + " " + Date.ToShortDateString();
        }
    }
}
