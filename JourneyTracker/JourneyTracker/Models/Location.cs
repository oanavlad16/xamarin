﻿using System;

namespace JourneyTracker.Models
{
    public class Location
    {
        public String Name { get; set; }
        public String Vicinity { get; set; }
        public String Icon { get; set; }
    }
}
