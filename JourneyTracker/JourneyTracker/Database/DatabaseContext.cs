﻿using JourneyTracker.Models;
using SQLite;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace JourneyTracker.Database
{
    public class DatabaseContext
    {
        SQLiteConnection connection;

        private static DatabaseContext instance = null;
        public static DatabaseContext GetInstance()
        {
            if (instance == null)
                instance = new DatabaseContext();
            return instance;
        }
        private DatabaseContext()
        {
            string path = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
            string dbName = "JourneyTracker.db";
            string connString = Path.Combine(path, dbName);
            connection = new SQLiteConnection(connString);
            connection.CreateTable<Journey>();
        }
        public int AddJourney(Journey c)
        {
            return connection.Insert(c);
        }

        public List<Journey> GetAll()
        {
            return connection.Query<Journey>("Select * from Journey");
        }

        public List<Journey> GatAllByDate(string data)
        {
            return connection.Query<Journey>("Select * from Journey where Data=?", data);
        }

        public int StergeToateInregistrarile()
        {
            return connection.DeleteAll<Journey>();
        }
    }
}
