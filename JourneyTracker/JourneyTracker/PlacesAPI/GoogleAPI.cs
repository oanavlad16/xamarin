﻿using JourneyTracker.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using System.Xml.XPath;
using Xamarin.Essentials;
using Location = JourneyTracker.Models.Location;

namespace JourneyTracker.PlacesAPI
{
    public class GoogleAPI
    {
        static readonly HttpClient client = new HttpClient();

        private static Location readSubtree(XmlReader reader)
        {
            Location l = new Location();

            while (reader.Read())
            {
                if (reader.IsStartElement())
                {
                    switch (reader.Name)
                    {
                        case "name":
                            reader.Read();
                            l.Name = reader.Value;
                            break;
                        case "vicinity":
                            reader.Read();
                            l.Vicinity = reader.Value;
                            break;
                        case "icon":
                            reader.Read();
                            l.Icon = reader.Value;
                            break;
                    }
                }
            }

            return l;
        }

        private static List<Location> PreiaCursDinXML(Stream streamXML)
        {
            List<Location> listaCursuri = new List<Location>();
            XmlReader reader = XmlReader.Create(streamXML);

            while (reader.Read())
            {
                if (reader.IsStartElement())
                {
                    switch (reader.Name.ToString())
                    {
                        case "result":
                            Location l = readSubtree(reader.ReadSubtree());
                            listaCursuri.Add(l);
                            break;
                    }

                }

            }
            return listaCursuri;
        }

        public static async Task<List<Location>> InitializeazaCurs(String keyword)
        {
            List<Location> listaCursuri = null;
            try
            {
                String APIkey = "AIzaSyDJ-4Uoqr5h35oWsH2OLNBgmwgXjelb06I";
                String radius = Preferences.Get("radius", 10).ToString();
                String lat = Preferences.Get("lat", 44.177269).ToString().Replace(",",".");
                String lng = Preferences.Get("lng", 28.652880).ToString().Replace(",", ".");
                HttpResponseMessage raspuns = await client.GetAsync(String.Format("https://maps.googleapis.com/maps/api/place/nearbysearch/xml?location={0},{1}&radius={2}&keyword={3}&key={4}", lat, lng, radius, keyword,APIkey));
                Stream continut = await raspuns.Content.ReadAsStreamAsync();
                listaCursuri = GoogleAPI.PreiaCursDinXML(continut);
            }
            catch (HttpRequestException e)
            {
                Console.WriteLine("\nException Caught!");
                Console.WriteLine("Message :{0} ", e.Message);
            }
            return listaCursuri;
        }
    }
}
